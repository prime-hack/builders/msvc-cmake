FROM msvc-wine:latest

RUN apt-get update && \
    apt-get install -y git cmake ninja-build  --no-install-recommends && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /opt/msvc/find_root/{x86,x64,arm,arm64}

COPY dxsdk /opt/msvc/dxsdk
COPY *.cmake /opt/msvc/

ENV DXSDK_DIR "/opt/msvc/dxsdk"
