## Build

First build **[wine-msvc](https://github.com/mstorsjo/msvc-wine)** 

```shell
docker build -t msvc:dx2005 .
```

## Usage

### Configure project

```shell
docker run --rm -v /path/to/src:/src -it msvc:dx2005 /bin/cmake -DCMAKE_TOOLCHAIN_FILE=/opt/msvc/x86.cmake -DCMAKE_BUILD_TYPE=Release -G Ninja -S /src -B /src/build
```

**Where:**

- **/path/to/src** - host path to sources
- **x86.cmake** - toolchain for arch **x86**

**Available arch:**

- x86
- x64
- arm
- arm64

**NOTE:** only **ninja** and **nmake** build systems are pre-installed in docker, but linux-version of CMake does not support nmake

### Build project

```shell
docker run --rm -v /path/to/src:/src -it msvc:dx2005 /bin/cmake --build /src/build
```

**Where:**

- **/path/to/src** - host path to sources

## Issues

- Debug build is fails with reason `fatal error C1902: Program database manager mismatch;  please check your installation`
